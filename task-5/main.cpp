#include "asserts.h"
#include "barrier.h"
#include "executor.h"
#include "flaky_atomic.h"
#include "program_options.h"
//#include "ya_contest_sim.h"

#include "solution.h"
//#include "tas_spinlock.h"

#include <atomic>
#include <string>
#include <sstream>
#include <vector>
#include <thread>

//////////////////////////////////////////////////////////////////////

template <typename SpinLock>
class MutexTester {
public:
    MutexTester(const size_t num_threads, const size_t num_iterations, const size_t num_shared_counters)
            : num_threads_{num_threads},
              num_iterations_{num_iterations},
              num_shared_counters_{num_shared_counters},
              start_barrier_{num_threads},
              locked_{false},
              shared_counters_(num_shared_counters, 0) {
    }

    void operator ()() {
        RunTest();
    }

private:
    void RunTest() {
        RunContenderThreads();
        ValidateCounterValues();
    }

    void RunContenderThreads() {
        TaskExecutor executor;
        for (size_t t = 0; t < num_threads_; ++t) {
            executor.Execute([this]() {
                RunContenderThread();
            });
        }
    }

    void RunContenderThread() {
        start_barrier_.Pass();

        for (size_t i = 0; i < num_iterations_; ++i) {
            typename SpinLock::Guard guard(spinlock_);

            // critical section

            test_assert(!locked_.exchange(true), "mutual exclusion violated");

            for (size_t& counter : shared_counters_) {
                volatile size_t temp = counter;
                temp += 1;
                std::this_thread::yield();
                counter = temp;
            }

            test_assert(locked_.exchange(false), "mutual exclusion violated");
        }
    }

    void ValidateCounterValues() {
        const size_t expected_value = num_iterations_ * num_threads_;

        for (size_t value : shared_counters_) {
            test_assert(value == expected_value, "unexpected counter value: " << value << ", expected " << expected_value);
        }
    }

private:
    size_t num_threads_;
    size_t num_iterations_;
    size_t num_shared_counters_;

    SpinLock spinlock_{};

    OnePassBarrier start_barrier_;

    std::atomic<bool> locked_;
    std::vector<size_t> shared_counters_;
};

///////////////////////////////////////////////////////////////////////

template <typename T> using Atomic = FlakyAtomic<T, 3>;
//template <typename T> using Atomic = std::atomic<T>;

void RunTest(int argc, char* argv[]) {
    size_t num_threads;
    size_t num_iterations;
    size_t num_shareds;

    read_opts(argc, argv, num_threads, num_iterations, num_shareds);

    //SolutionTests::SimulateYandexContest();

    MutexTester<SpinLock<Atomic>>{num_threads, num_iterations, num_shareds}();
}

int main(int argc, char* argv[]) {
    RunTest(argc, argv);
    return EXIT_SUCCESS;
}