#pragma once

#include <atomic>
#include <thread>

//////////////////////////////////////////////////////////////////////

template <typename T, size_t YieldFreq = 3>
class FlakyAtomic {
public:
    FlakyAtomic(const T& initial_value)
            : register_(initial_value) {
    }

    T load() const {
        Yield();
        const T value = register_.load();
        Yield();
        return value;
    }

    void store(const T& value) {
        Yield();
        register_.store(value);
        Yield();
    }

    T operator =(const T& value) {
        store(value);
        return value;
    }

    T exchange(const T& new_value) {
        Yield();
        const T prev_value = register_.exchange(new_value);
        Yield();
        return prev_value;
    }

    bool compare_exchange_weak(T& expected, const T& desired) {
        Yield();
        const bool succeeded = register_.compare_exchange_weak(expected, desired);
        Yield();
        return succeeded;
    }

    bool compare_exchange_strong(T& expected, const T& desired) {
        Yield();
        const bool succeeded = register_.compare_exchange_strong(expected, desired);
        Yield();
        return succeeded;
    }

private:
    void Yield() const {
        static std::atomic<size_t> called{0};

        if (called.fetch_add(1) % YieldFreq == 0) {
            std::this_thread::yield();
        }
    }

private:
    std::atomic<T> register_;
};

//////////////////////////////////////////////////////////////////////
