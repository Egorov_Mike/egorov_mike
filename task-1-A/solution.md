Задача А. Алгоритм Петерсона
    Потоки 0 и 1 могут попасть в критическую секцию в один момент времени:
если 0 войдет в lock(), установит victim в 0, потом зайдет поток 1, установит
victim в 1, want[1] в true и зайдет в критическую секцию, после этого поток 0
установит want[0] в true и тоже зайдет в критическую секцию, так как условие
в цикле while (want[1] and victim == 0) не выполняется (в данный момент
victim ==1).