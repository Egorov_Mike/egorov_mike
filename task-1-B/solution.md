Задача B. Tricky Mutex
    Tricky mutex гарантирует взаимное ислючение.
    Если один поток уже в критической секции, то другой поток в неё уже не попадет, пока
первый не выйдет; если потоки одновременно зашли в lock, то оба они не могут зайти в критическую
секцию, так как fetch_add - атомарная операция.
    Tricky mutex не гарантирует свободу от взаимной блокировки.
    Пусть два потока (0 и 1) зайдут в lock одновременно, когда третий поток уже занял критическую секцию.
Поток 0 проверит условие в while, войдет в тело цикла, но еще не выполнит команду из цикла, в это
время третий поток выйдет из критической секции, вызвав unlock. Поток 1 проверит условие в while,
войдет в тело цикла, но еще не выполнит команду из цикла, в это время поток 0 выйдет из тела
while и снова войдет в тело цикла. В итоге два потока могут вечно находиться в цикле while. Такая
ситуация называется лайвлоком, два потока могут вечно выполнять бесполезные действия. Наличие
лайвлока говорит о том, что алгоритм не гарантирует свободу от взаимной блокировки.